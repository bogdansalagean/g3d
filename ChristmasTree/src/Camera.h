#pragma once

//openGL
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#define GLM_FORCE_CTOR_INIT 
#include <glm/GLM.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

enum class CameraMovement {
	Forward,
	Backward,
	Left,
	Right,
	Up,
	Down
};

class Camera
{
public:
	//Mouse position
	float m_lastX;
	float m_lastY;
	bool m_firstMouseMovement;

	//Frame and timing
	float m_deltaTime;
	float m_lastFrame;
public:
	static const float YAW;
	static const float PITCH;
	static const float SPEED;
	static const float SENSITIVITY;
	static const float ZOOM;
public:
	// Camera Attributes
	glm::vec3 Position;
	glm::vec3 Front;
	glm::vec3 Up;
	glm::vec3 Right;
	glm::vec3 WorldUp;
	// Euler Angles
	float Yaw;
	float Pitch;
	// Camera options
	float MovementSpeed;
	float MouseSensitivity;
	float Zoom;


	// Constructor with vectors
	Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), float yaw = YAW, float pitch = PITCH);
	// Constructor with scalar values
	Camera(float posX, float posY, float posZ, float upX, float upY, float upZ, float yaw, float pitch);

	// Returns the view matrix calculated using Euler Angles and the LookAt Matrix
	glm::mat4 GetViewMatrix();
	// Processes input received from any keyboard-like input system. Accepts input parameter in the form of camera defined ENUM (to abstract it from windowing systems)
	void ProcessKeyboard(CameraMovement direction, float deltaTime);

	// Processes input received from a mouse input system. Expects the offset value in both the x and y direction.
	void ProcessMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch = true);

	// Processes input received from a mouse scroll-wheel event. Only requires input on the vertical wheel-axis
	void ProcessMouseScroll(float yoffset);


public:
	//Setter and getters
	void SetCursorXPosition(float xPositon);
	void SetCursorYPosition(float yPosition);
	float GetCursorXPosition() const;
	float GetCursorYPosition() const;

	void SetDeltaTime(float deltaTime);
	void SetCurrentFrame(float frame);
	float GetDeltaTime() const;
	float GetLastFrame() const;

	void initMousePosition();
	void initFrame();
	void setFirstMouseMovement(bool value);
	bool isFirstMouseMovement() const;

private:
	// Calculates the front vector from the Camera's (updated) Euler Angles
	void updateCameraVectors();
};

