#include "Camera.h"

const float Scale = 10.0f;
const float Camera::YAW = -90.0f;
const float Camera::PITCH = 0.0f;
const float Camera::SPEED = 20.0f / Scale;
const float Camera::SENSITIVITY = 0.1f;
const float Camera::ZOOM = 45.0f;
const glm::vec3 Offset = glm::vec3(0.0f, -11.75f, 0.0f);

Camera::Camera(glm::vec3 position, glm::vec3 up, float yaw, float pitch) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVITY), Zoom(ZOOM)
{
	initMousePosition();
	Position = position;
	WorldUp = up;
	Yaw = yaw;
	Pitch = pitch;
	updateCameraVectors();
}

Camera::Camera(float posX, float posY, float posZ, float upX, float upY, float upZ, float yaw, float pitch) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(), MouseSensitivity(SENSITIVITY), Zoom(ZOOM)
{
	initMousePosition();
	Position = glm::vec3(posX, posY, posZ);
	WorldUp = glm::vec3(upX, upY, upZ);
	Yaw = yaw;
	Pitch = pitch;
	updateCameraVectors();
}

glm::mat4 Camera::GetViewMatrix()
{
	return glm::lookAt(Position, Position + Front, Up);
}

template <class T>
T clamp(T x, T min, T max)
{
	return glm::min(glm::max(min, x), max);
}

void setInBounds(glm::vec3 &Position)
{
	Position.x = clamp(Position.x, -800.0f / Scale, 800.0f / Scale);
	Position.y = clamp(Position.y, -30.0f, 400.0f / Scale);
	Position.z = clamp(Position.z, -800.0f / Scale, 800.0f/ Scale);
}

void Camera::ProcessKeyboard(CameraMovement direction, float deltaTime)
{
	float velocity = MovementSpeed * deltaTime * 30;
	switch (direction)
	{
	case CameraMovement::Forward:
		Position += Front * velocity;
		break;
	case CameraMovement::Backward:
		Position -= Front * velocity;
		break;
	case CameraMovement::Left:
		Position -= Right * velocity;
		break;
	case CameraMovement::Right:
		Position += Right * velocity;
		break;
	case CameraMovement::Up:
		Position += Up * velocity;
		break;
	case CameraMovement::Down:
		Position -= Up * velocity;
		break;
	default:
		break;
	}
	setInBounds(Position);
}

void Camera::ProcessMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch)
{
	xoffset *= MouseSensitivity;
	yoffset *= MouseSensitivity;

	Yaw += xoffset;
	Pitch += yoffset;

	// Make sure that when pitch is out of bounds, screen doesn't get flipped
	if (constrainPitch)
	{
		if (Pitch > 89.0f)
			Pitch = 89.0f;
		if (Pitch < -89.0f)
			Pitch = -89.0f;
	}

	// Update Front, Right and Up Vectors using the updated Euler angles
	updateCameraVectors();
}

void Camera::ProcessMouseScroll(float yoffset)
{
	if (Zoom >= 1.0f && Zoom <= 45.0f)
		Zoom -= yoffset;
	if (Zoom <= 1.0f)
		Zoom = 1.0f;
	if (Zoom >= 45.0f)
		Zoom = 45.0f;
}

void Camera::SetCursorXPosition(float xPositon)
{
	this->m_lastX = xPositon;
}

void Camera::SetCursorYPosition(float yPosition)
{
	this->m_lastY = yPosition;
}

float Camera::GetCursorXPosition() const
{
	return this->m_lastX;
}

float Camera::GetCursorYPosition() const
{
	return this->m_lastY;
}

void Camera::SetDeltaTime(float deltaTime)
{
	this->m_deltaTime = deltaTime;
}

void Camera::SetCurrentFrame(float frame)
{
	this->m_lastFrame = frame;
}

float Camera::GetDeltaTime() const
{
	return this->m_deltaTime;
}

float Camera::GetLastFrame() const
{
	return this->m_lastFrame;
}

void Camera::initMousePosition()
{
	this->m_lastX = 0.0f;
	this->m_lastY = 0.0f;
	this->m_firstMouseMovement = true;
}

void Camera::initFrame() 
{
	this->m_deltaTime = 0.0f;
	this->m_lastFrame = 0.0f;
}

void Camera::setFirstMouseMovement(bool value)
{
	this->m_firstMouseMovement = value;
}

bool Camera::isFirstMouseMovement() const
{
	return this->m_firstMouseMovement;
}

void Camera::updateCameraVectors()
{
	// Calculate the new Front vector
	glm::vec3 front;
	front.x = cos(glm::radians(Yaw)) * cos(glm::radians(Pitch));
	front.y = sin(glm::radians(Pitch));
	front.z = sin(glm::radians(Yaw)) * cos(glm::radians(Pitch));
	Front = glm::normalize(front);
	// Also re-calculate the Right and Up vector
	Right = glm::normalize(glm::cross(Front, WorldUp));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
	Up = glm::normalize(glm::cross(Right, Front));
}