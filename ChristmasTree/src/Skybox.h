#pragma once

#include <vector>
#include <string>

//OpenGL
#include "Shader.h"

class Skybox
{
private:
	unsigned int Skybox::loadCubemap(const std::vector<std::string> & faces) const;

public:
	Skybox(const Shader & shader);
	~Skybox();
public:
	void renderSkybox(glm::mat4 & cameraViewMatrix, glm::mat4 & cameraProjectionMatrix);
private:
	size_t VBO, VAO;
	size_t texture;
	Shader m_shader;
};