#pragma once

#include <iostream>
#include <string>
#include <vector>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Camera.h"
#include "Shader.h"
#include "Model.h"
#include "Skybox.h"

class Window
{
	GLFWwindow * m_window;
	size_t m_screenWidth;
	size_t m_screenHeight;

private:
	void initWindow();
	void processInput();
	void render(Shader & shader, Model & tree, Model & house, Model & santa, Model & gift, Model & lamp, Model & house2);
public:
	Window(size_t screenWidth = 1920, size_t screenHeight = 1080);
	~Window();

public:
	void run();

public:
	GLFWwindow * getWindow();

};

