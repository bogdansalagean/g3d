#include "Window.h"

unsigned int planeVAO = 0;
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));

void framebufferSizeCallback(GLFWwindow * window, int width, int height);
void mouseCallback(GLFWwindow * window, double xpos, double ypos);
void scrollCallback(GLFWwindow * window, double xoffset, double yoffset);

float Ka = 0.5;
float Kd = 0.5;
float Kr = 0.5;
float Kr2 = 2.0;

Window::Window(size_t screenWidth, size_t screenHeight)
{
	this->m_screenWidth = screenWidth;
	this->m_screenHeight = screenHeight;
	camera.SetCursorXPosition(this->m_screenWidth / 2.0f);
	camera.SetCursorYPosition(this->m_screenHeight / 2.0f);
	if (!glfwInit())
	{
		std::cerr << "Failed to initialize GLFW" << std::endl;
		assert(0);
	}
	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	initWindow();
}

void Window::initWindow()
{
	this->m_window = glfwCreateWindow(this->m_screenWidth, this->m_screenHeight, "Christmas Tree", NULL, NULL);
	if (this->m_window == NULL)
	{
		std::cerr << "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials" << std::endl;
		glfwTerminate();
		assert(0);
	}
	glfwMakeContextCurrent(this->m_window);
	glfwSetFramebufferSizeCallback(this->m_window, framebufferSizeCallback);
	glfwSetCursorPosCallback(this->m_window, mouseCallback);
	glfwSetScrollCallback(this->m_window, scrollCallback);

	// tell GLFW to capture our mouse
	glfwSetInputMode(this->m_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	// Initialize GLEW
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return;
	}
	glEnable(GL_DEPTH_TEST);
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

Window::~Window()
{
	// Close OpenGL window and terminate GLFW
	glfwTerminate();
}

void drawBlendings(Shader& shaderBlending, glm::mat4 model)
{
	for (int j = 1; j < 6; j++)
	{
		model = glm::rotate(model, glm::radians(j * 30.f), glm::vec3(0.0f, 1.0f, 0.0f));
		shaderBlending.setMat4("model", model);
		glDrawArrays(GL_TRIANGLES, 0, 6);
	}

}


void Window::run()
{
	// tell GLFW to capture our mouse
	glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// glad: load all OpenGL function pointers
	// ---------------------------------------
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return;
	}

	// configure global opengl state
	// -----------------------------
	glEnable(GL_DEPTH_TEST);

	// build and compile shaders
	// -------------------------

	glm::vec3 lightPos(0.0f, 0.0f, 2.0f);

	Shader ourShader("../src/model_loading.vs", "../src/model_loading.fs");
	
	Skybox skybox(Shader("../src/skybox.vs", "../src/skybox.fs"));

	Shader lightingShader("../src/ShadowMappingDepth.vs", "../src/ShadowMappingDepth.fs");

	Shader shaderBlending("../src/Blending.vs", "../src/Blending.fs");
	shaderBlending.setInt("texture1", 0);

	// configure depth map FBO
	// -----------------------
	const unsigned int SHADOW_WIDTH = 16384, SHADOW_HEIGHT = 16384;
	unsigned int depthMapFBO;
	glGenFramebuffers(1, &depthMapFBO);
	// create depth texture
	unsigned int depthMap;
	glGenTextures(1, &depthMap);
	glBindTexture(GL_TEXTURE_2D, depthMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	float borderColor[] = { 1.0, 1.0, 1.0, 1.0 };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
	// attach depth texture as FBO's depth buffer
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// shader configuration
	// --------------------
	
	shaderBlending.use();
	shaderBlending.setInt("shadowMap", 1);
	ourShader.use();
	ourShader.setInt("shadowMap", 1);

	glEnable(GL_CULL_FACE);

	// load models
	// -----------
	Model christmasTree("../resources/objects/ct2/ct2.obj");
	Model house("../resources/objects/st/WoodHouse.obj");
	Model santa("../resources/objects/santa/santa.obj");
	Model gift("../resources/objects/gift/gifts.obj");
	Model lamp("../resources/objects/Lamp/StreetLamp.obj");
	Model house2("../resources/objects/house2/house2.obj");

	// draw in wireframe
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	// render loop
	// -----------
	while (!glfwWindowShouldClose(m_window))
	{
		// per-frame time logic
		// --------------------
		float currentFrame = static_cast<float>(glfwGetTime());
		camera.SetDeltaTime(currentFrame - camera.GetLastFrame());
		camera.SetCurrentFrame(currentFrame);

		// input
		// -----
		processInput();

		// render
		// ------
		glClearColor(0.05f, 0.05f, 0.05f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glViewport(0, 0, this->m_screenWidth, this->m_screenHeight);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		// don't forget to enable shader before setting uniforms
		//lightingShader.use();
		//lightingShader.setVec3("objectColor", 0.5f, 1.0f, 0.31f);
		//lightingShader.setVec3("lightColor", 1.0f, 1.0f, 1.0f);
		//lightingShader.setVec3("lightPos", lightPos);
		////lightingShader.setVec3("viewPos", pCamera->GetPosition());
		//lightingShader.setFloat("Ka", Ka);
		//lightingShader.setFloat("Kd", Kd);
		//lightingShader.setFloat("n", Kr);
		//lightingShader.setFloat("kr", 2);
		shaderBlending.use();
		ourShader.use();


	
		

		// view/projection transformations
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)m_screenWidth / (float)m_screenHeight, 0.1f, 1000.0f);
		glm::mat4 view = camera.GetViewMatrix();
		ourShader.setMat4("projection", projection);
		ourShader.setMat4("view", view);
		//ourShader2.setMat4("projection", projection);
		//ourShader2.setMat4("view", view);

		//glm::mat4 model = glm::scale(glm::mat4(1.0), glm::vec3(3.0f));
		//lightingShader.setMat4("model", model);

		render(ourShader, christmasTree, house, santa, gift, lamp, house2);

		// draw skybox
		skybox.renderSkybox(view, projection);

		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		// -------------------------------------------------------------------------------
		glfwSwapBuffers(m_window);
		glfwPollEvents();
	}
}

void Window::render(Shader & shader, Model & tree, Model & house, Model & santa, Model & gift , Model & lamp , Model & house2)
{


	// draw house
	glm::mat4 model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(0.0f, -50.0f, 55.0f)); // translate it down so it's at the center of the scene
	model = glm::scale(model, glm::vec3(1.0f, 1.0f, 1.0f));	// it's a bit too big for our scene, so scale it down
	//model = glm::rotate<float>(model, glm::radians(180.f), glm::vec3(0.0f, 1.0f, 0.0f));
	shader.setMat4("model", model);
	house2.Draw(shader);

	// draw house
	glm::mat4 model6 = glm::mat4(1.0f);
	model6 = glm::translate(model6, glm::vec3(0.0f, -50.0f, 0.0f)); // translate it down so it's at the center of the scene
	model6 = glm::scale(model6, glm::vec3(0.09f, 0.09f, 0.09f));	// it's a bit too big for our scene, so scale it down
	shader.setMat4("model", model6);
	house.Draw(shader);

	// draw tree
	glm::mat4 model1 = glm::mat4(1.0f);
	model1 = glm::translate(model1, glm::vec3(0.0f, -50.0f, 0.0f));
	model1 = glm::scale(model1, glm::vec3(0.25f, 0.25f, 0.25f));
	model1 = glm::rotate<float>(model1, glm::radians(90.f), glm::vec3(-1.0f, 0.0f, 0.0f));
	shader.setMat4("model", model1);
	tree.Draw(shader);

	//draw santa
	glm::mat4 model2 = glm::mat4(1.0f);
	model2 = glm::translate(model2, glm::vec3(0.0f, -50.0f, 20.0f));
	model2 = glm::scale(model2, glm::vec3(0.05f, 0.05f, 0.05f));
	model2 = glm::rotate<float>(model2, glm::radians(90.f), glm::vec3(-1.0f, 0.0f, 0.0f));
	shader.setMat4("model", model2);
	santa.Draw(shader);

	//draw gift
	glm::mat4 model3 = glm::mat4(1.0f);
	model3 = glm::translate(model3, glm::vec3(3.0f, -50.0f, 6.0f));
	model3 = glm::scale(model3, glm::vec3(0.05f, 0.05f, 0.05f));
	model3 = glm::rotate<float>(model3, glm::radians(90.f), glm::vec3(-1.0f, 0.0f, 0.0f));
	shader.setMat4("model", model3);
	gift.Draw(shader);

	//draw lamp
	glm::mat4 model4 = glm::mat4(1.0f);
	model4 = glm::translate(model4, glm::vec3(25.0f, -42.0f, 22.0f));
	model4 = glm::scale(model4, glm::vec3(0.6f, 0.6f, 0.6f));
	shader.setMat4("model", model4);
	lamp.Draw(shader);

	//draw lamp
	glm::mat4 model5 = glm::mat4(1.0f);
	model5 = glm::translate(model5, glm::vec3(-25.0f, -42.0f, 22.0f));
	model5 = glm::scale(model5, glm::vec3(0.6f, 0.6f, 0.6f));
	shader.setMat4("model", model5);
	lamp.Draw(shader);

	//draw lamp
	glm::mat4 model7 = glm::mat4(1.0f);
	model7 = glm::translate(model7, glm::vec3(-25.0f, -42.0f, -22.0f));
	model7 = glm::scale(model7, glm::vec3(0.6f, 0.6f, 0.6f));
	model7 = glm::rotate<float>(model7, glm::radians(180.f), glm::vec3(0.0f, 1.0f, 0.0f));
	shader.setMat4("model", model7);
	lamp.Draw(shader);

	//draw lamp
	glm::mat4 model8 = glm::mat4(1.0f);
	model8 = glm::translate(model8, glm::vec3(25.0f, -42.0f, -22.0f));
	model8 = glm::scale(model8, glm::vec3(0.6f, 0.6f, 0.6f));
	model8 = glm::rotate<float>(model8, glm::radians(180.f), glm::vec3(0.0f, 1.0f, 0.0f));
	shader.setMat4("model", model8);
	lamp.Draw(shader);
}



void framebufferSizeCallback(GLFWwindow * window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and 
	// height will be significantly larger than specified on retina displays.
	glViewport(0, 0, width, height);
}

void mouseCallback(GLFWwindow * window, double xpos, double ypos)
{
	if (camera.isFirstMouseMovement())
	{
		camera.SetCursorXPosition(static_cast<float>(xpos));
		camera.SetCursorYPosition(static_cast<float>(ypos));
		camera.setFirstMouseMovement(false);
	}

	float xoffset = static_cast<float>(xpos - camera.GetCursorXPosition());
	float yoffset = static_cast<float>(camera.GetCursorYPosition() - ypos); // reversed since y-coordinates go from bottom to top

	camera.SetCursorXPosition(static_cast<float>(xpos));
	camera.SetCursorYPosition(static_cast<float>(ypos));

	camera.ProcessMouseMovement(xoffset, yoffset);
}

void scrollCallback(GLFWwindow * window, double xoffset, double yoffset)
{
	camera.ProcessMouseScroll(static_cast<float>(yoffset));
}

void Window::processInput()
{
	if (glfwGetKey(this->m_window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(this->m_window, true);

	float deltaTime = camera.GetDeltaTime();

	if (glfwGetKey(m_window, GLFW_KEY_W) == GLFW_PRESS)
		camera.ProcessKeyboard(CameraMovement::Forward, deltaTime);
	if (glfwGetKey(m_window, GLFW_KEY_S) == GLFW_PRESS)
		camera.ProcessKeyboard(CameraMovement::Backward, deltaTime);
	if (glfwGetKey(m_window, GLFW_KEY_A) == GLFW_PRESS)
		camera.ProcessKeyboard(CameraMovement::Left, deltaTime);
	if (glfwGetKey(m_window, GLFW_KEY_D) == GLFW_PRESS)
		camera.ProcessKeyboard(CameraMovement::Right, deltaTime);
}

GLFWwindow * Window::getWindow()
{
	return this->m_window;
}